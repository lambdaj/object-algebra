Object Algebra in Java
=====================

This project was heavily inspired by the following [paper](https://www.cs.utexas.edu/~wcook/Drafts/2012/ecoop2012.pdf).  In summary, 
an Object Algebra is a solution to the [Expression Problem](https://en.wikipedia.org/wiki/Expression_problem) that provides the following
benefits 

+   uses simple generic types  
+   requires no modifications to the Java language

Note that my contributions are 

+   Updates the code for Java 8 (taking advantage of lambda syntax).   
+   Provides a base framework for implementing new Object Algebras via the *ObjectAlgebraDef* interface which will be explained more thoroughly below

##### ObjectAlgebraDef explanation

The definition of *ObjectAlgebraDef* is as follows 
```java
public interface ObjectAlgebraDef<RootType, R> extends Function<RootType, R> { ... }
```
which is a 'Type Alias' for *Function<RootType, R>*, where 

+   *RootType* is the type of the common interface for this ObjectAlgebra.  If there is not a root type for this ObjectAlgebra consider using *NoRootType*
+   *R* is result type for this ObjectAlgebra 

A following example should solidify the readers understanding of *RootType* and *R*.  We will use the following classes throughout examples in this documentation:

```java
public interface Tree {}

public class Fork implements Tree {
	private final int value;
	private final Tree left;
	private final Tree right;
	
	// Constructors, getters, equals/hashCode/toString omitted for conciceness
}

public class Leaf implements Tree {
	private final int value;
	
	// Constructors, getters, equals/hashCode/toString omitted for conciceness
}
```

where a Tree *ObjectAlgebra* for operations on Tree instances is 
```java
public interface TreeOAD<R> extends ObjectAlgebraDef<Tree, R> {
	R visit(final Fork fork);
	R visit(final Leaf leaf);
}
```

and suppose (keeping it simple for the example) we are required to implement an operation *pprint* on *Tree* such that when *pprint* is applied to a *Fork* instance 
the text "fork" is returned, and if *pprint* is applied to a *Leaf* instance the text "leaf" is returned.  This can be implemented by creating the following class
 
```java
public class PPrint implements TreeOAD<String> {
	@Override
	public String visit(final Fork fork) {
		return "fork";
	}
	
	@Override
	public String visit(final Leaf leaf) {
		return "leaf";
	}
}
```

It is up to the developer if they wish to make *PPrint* a singleton or wire it in a Spring Configuration class, etc...  But the fact the operation can be implemented is
what is important for this project.

Next consider adding a new operation on *Tree* called *isLeaf* (once again keeping the example simple for now) 
 
```java
public class IsLeaf implements TreeOAD<Boolean> {
	@Override
	public Boolean visit(final Fork fork) {
		return false;
	}
	
	@Override
	public Boolean visit(final Leaf leaf) {
		return true;
	}
}
```

And now we consider adding a new type of *Tree* called *TriFork* 

```java
public class TriFork implements Tree {
	private final int value;
	private final Tree left;
	private final Tree middle;
	private final Tree right;
	
	// Constructors, getters, equals/hashCode/toString omitted for conciceness
}
```

and extending *pprint* to now operate on *TriFork* instances involves creating the following classes
```java
public interface TreeOAD_V2<R> extends ObjectAlgebraDef<Tree, R> {
	R visit(final TriFork triFork);
}

public class PPrint_V2 extends PPrint implements TreeOAD_V2<String> {
	@Override
	public String visit(final TriFork triFork) {
		return "trifork";
	}
}
```

Note that it is not the concern of this paper for how the 'versioning'/extension of existing operations should be coded and will likely vary between
projects.  Now you have seen simple examples of adding new data type(s) and extending/adding operation(s).  From these examples we can see the following benefits 
to using *ObjectAlgebras* 

+ new operations can be implemented by __reusing__ existing code __without modification__
+ new data types (implementations of the *RootType* ) can be added at any time.  If you are wondering how an existing operation can 'handle' a new data type keep reading!

Before more complex examples are shown a further explanation of the methods defined in *ObjectAlgebraDef*

```java
default String visitMethodName() {
	return "visit";
}

R visitDefault(final RootType r);

default R apply(final RootType r) {
	if (r == null) {
		LOGGER.debug("supplied argument null!  Not able to dispatch by type therefore returning null.");
		return null;
	}

	final String methodName = visitMethodName();
	final Class<?> rClass = r.getClass();
	try {
		@SuppressWarnings("unchecked")
		final R result = (R) this.getClass().getMethod(methodName, rClass).invoke(this, r);
		return result;
	} catch (final NoSuchMethodException e) {
		LOGGER.warn(format("Unable to find the method with name: %s with argument of type: %s", methodName, rClass.getName()), e);
		return visitDefault(r);
	} catch (final InvocationTargetException | IllegalAccessException e) {
		LOGGER.error(format("Unable to invoke method with name: %s with argument of type: %s", methodName, rClass.getName()), e);
		throw new ObjectAlgebraApplyException(e);
	}
}
```

where *apply* is defined as a default method that takes in a parameter of type *RootType*.  Think of this method as a catch-all method that will 
delegate to the correct 'visit' method by inspecting the instance type of the parameter if it is not null.  If no such 'visit' method can be found
then the result of calling *visitDefault(r)* is returned.  The last method explained is *visitMethodName()* and the result of calling this method is 
the method name used by *apply* when attempting to find the method of the correct type via reflection.


The *apply* method is particularly useful when defining recursive operations such as tree size
 
```java
public class Size implements TreeOAD_V2<Integer> {
	@Override
	public Integer visit(final Fork fork) {
		return 1 + this.apply(fork.getLeft()) + this.apply(fork.getRight());
	}
	
	@Override
	public Integer visit(final Leaf leaf) {
		return 1;
	}
	
	@Override
	public Integer visit(final TriFork fork) {
		return 1 + this.apply(fork.getLeft()) + this.apply(fork.getMiddle()) + this.apply(fork.getRight());
	}
}
```

Notice the use of *apply* to handle *Fork* / *TriFork* branches.  The types of the branch fields are *Tree* and if *apply* did not exist then the Size operation
could not be implemented as simply as it is above!

Any data type that does not have a corresponding 'visit' method will end up being handled by *visitDefault*.
  
##### More advance usage 

Consider an operation called *pprint* that will print the value and then indent the results of applying *pprint* to each child below the value.  *pprint* applied 
to a *Leaf* node will just print the value.

```java
public class PPrint implements TreeOAD_V2<Function<Integer, String>> {
	private static final String INDENT = "  ";

	@Override
	public Function<Integer, String> visit(final Fork fork) {
		return depth -> String.format("%s%s%n%s%n%s%n", 
			indent(depth), 
			Objects.toString(fork.getValue()),
			this.apply(fork.getLeft()).apply(depth + 1),
			this.apply(fork.getRight()).apply(depth + 1));
	}
	
	@Override
	public Function<Integer, String> visit(final Leaf leaf) {
		return depth -> indent(depth) + Objects.toString(fork.getValue());
	}
	
	@Override
	public Function<Integer, String> visit(final TriFork fork) {
		return depth -> String.format("%s%s%n%s%n%s%n%s%n", 
			indent(depth), 
			Objects.toString(fork.getValue()),
			this.apply(fork.getLeft()).apply(depth + 1),
			this.apply(fork.getMiddle()).apply(depth + 1),
			this.apply(fork.getRight()).apply(depth + 1));
	}
	
	private String indent(final int depth) {
		return IntStream.range(0, depth).collect(Collectors.joining(INDENT));
	}
}
```

Notice that the result of *PPrint* is a *Function<Integer, String>*.  This Function takes in the depth and pprints the tree according to that depth.