package io.bitbucket.lambdaj;

/**
 * Marker type to represent no root type for the ObjectAlgebra.  Cannot be extended nor instantiated
 */
public final class NoRootType {
  private NoRootType() {}
}
