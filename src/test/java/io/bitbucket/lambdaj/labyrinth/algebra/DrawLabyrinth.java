package io.bitbucket.lambdaj.labyrinth.algebra;

import io.bitbucket.lambdaj.labyrinth.cases.DeadEnd;
import io.bitbucket.lambdaj.labyrinth.cases.Fork;
import io.bitbucket.lambdaj.labyrinth.cases.Passage;

import java.util.Collections;
import java.util.function.Consumer;

public class DrawLabyrinth implements NodeObjectAlgebraDef<Consumer<Integer>> {
  @Override
  public Consumer<Integer> visit(final DeadEnd deadEnd) {
    return indentAmt -> System.out.println(indent(indentAmt) + "-");
  }

  @Override
  public Consumer<Integer> visit(final Passage passage) {
    return indentAmt -> System.out.print(String.join("\n", Collections.nCopies(passage.getLength(), indent(indentAmt) + "|")));
  }

  @Override
  public Consumer<Integer> visit(final Fork fork) {
    final DrawLabyrinth self = this;
    return indentAmt -> {
      System.out.println(indent(indentAmt) + "+");
      self.apply(fork.getLeft()).accept(indentAmt);
      System.out.println();
      System.out.println("---+");
      self.apply(fork.getRight()).accept(indentAmt + 1);
    };
  }

  private String indent(final int indentAmt) {
    return String.join("", Collections.nCopies(indentAmt, "   "));
  }
}