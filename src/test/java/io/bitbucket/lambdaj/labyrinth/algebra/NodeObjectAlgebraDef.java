package io.bitbucket.lambdaj.labyrinth.algebra;

import io.bitbucket.lambdaj.ObjectAlgebraDef;
import io.bitbucket.lambdaj.labyrinth.cases.DeadEnd;
import io.bitbucket.lambdaj.labyrinth.cases.Fork;
import io.bitbucket.lambdaj.labyrinth.cases.Node;
import io.bitbucket.lambdaj.labyrinth.cases.Passage;

public interface NodeObjectAlgebraDef<R> extends ObjectAlgebraDef<Node, R> {
  R visit(final DeadEnd deadEnd);
  R visit(final Passage passage);
  R visit(final Fork fork);

  @Override
  default R visitDefault(final Node r) {
    throw new UnsupportedOperationException("All cases are known, this should never happen!?");
  }
}
