package io.bitbucket.lambdaj.labyrinth;

import io.bitbucket.lambdaj.labyrinth.algebra.DrawLabyrinth;
import io.bitbucket.lambdaj.labyrinth.cases.DeadEnd;
import io.bitbucket.lambdaj.labyrinth.cases.Fork;
import org.junit.Test;

import static io.bitbucket.lambdaj.labyrinth.cases.Passage.passage;

public class DrawLabyrinthTest {
  private final DrawLabyrinth draw = new DrawLabyrinth();

  @Test
  public void testPassage() throws Exception {
    draw.visit(passage(4)).accept(0);
  }

  @Test
  public void testDeadEnd() throws Exception {
    draw.visit(DeadEnd.INSTANCE).accept(0);
  }

  @Test
  public void testFork() throws Exception {
    draw.visit(Fork.fork(passage(3), DeadEnd.INSTANCE)).accept(0);
  }
}
