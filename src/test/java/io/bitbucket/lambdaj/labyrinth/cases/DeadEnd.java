package io.bitbucket.lambdaj.labyrinth.cases;

public class DeadEnd implements Node {
  public static final DeadEnd INSTANCE = new DeadEnd();

  private DeadEnd() {

  }

  @Override
  public boolean equals(final Object o) {
    return INSTANCE == o;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public String toString() {
    return "DeadEnd{}";
  }
}
