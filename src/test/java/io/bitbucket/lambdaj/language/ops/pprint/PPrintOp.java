package io.bitbucket.lambdaj.language.ops.pprint;

import io.bitbucket.lambdaj.language.LanguageObjAlgDef;
import io.bitbucket.lambdaj.language.cases.Add;
import io.bitbucket.lambdaj.language.cases.Int;
import io.bitbucket.lambdaj.language.cases.Mul;

import static java.lang.String.format;

public class PPrintOp implements LanguageObjAlgDef<PPrint> {
  @Override
  public PPrint visit(final Int num) {
    return () -> Integer.toString(num.getValue());
  }

  @Override
  public PPrint visit(final Add add) {
    final PPrintOp self = this;
    return () -> format("%s + %s", self.apply(add.getX()).get(), self.apply(add.getY()).get());
  }

  @Override
  public PPrint visit(final Mul mul) {
    final PPrintOp self = this;
    return () -> format("%s * %s", self.apply(mul.getX()).get(), self.apply(mul.getY()).get());
  }
}