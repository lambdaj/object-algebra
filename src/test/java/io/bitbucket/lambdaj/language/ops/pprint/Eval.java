package io.bitbucket.lambdaj.language.ops.pprint;


import io.bitbucket.lambdaj.language.cases.Int;

import java.util.function.Supplier;

/**
 * Marker interface for the evaluation of expressions
 */
public interface Eval extends Supplier<Int> {

}
