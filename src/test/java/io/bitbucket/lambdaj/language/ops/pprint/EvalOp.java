package io.bitbucket.lambdaj.language.ops.pprint;

import io.bitbucket.lambdaj.language.LanguageObjAlgDef;
import io.bitbucket.lambdaj.language.cases.Add;
import io.bitbucket.lambdaj.language.cases.Int;
import io.bitbucket.lambdaj.language.cases.Mul;

public class EvalOp implements LanguageObjAlgDef<Eval> {
  @Override
  public Eval visit(final Int num) {
    return () -> num;
  }

  @Override
  public Eval visit(final Add add) {
    final EvalOp self = this;
    return () -> self.apply(add.getX()).get()
      .flatMap(x ->
        self.apply(add.getY()).get()
          .map(y -> x + y));
  }

  @Override
  public Eval visit(final Mul mul) {
    final EvalOp self = this;
    return () -> self.apply(mul.getX()).get()
      .flatMap(x ->
        self.apply(mul.getY()).get()
          .map(y -> x * y));
  }
}
