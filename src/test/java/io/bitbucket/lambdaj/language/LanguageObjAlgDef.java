package io.bitbucket.lambdaj.language;

import io.bitbucket.lambdaj.ObjectAlgebraDef;
import io.bitbucket.lambdaj.language.cases.Add;
import io.bitbucket.lambdaj.language.cases.Exp;
import io.bitbucket.lambdaj.language.cases.Int;
import io.bitbucket.lambdaj.language.cases.Mul;

public interface LanguageObjAlgDef<R> extends ObjectAlgebraDef<Exp, R> {
  R visit(final Int num);
  R visit(final Add add);
  R visit(final Mul mul);

  @Override
  default R visitDefault(final Exp r) {
    if (r == null) {
      throw new NullPointerException("Exp to visitDefault was null!");
    }
    throw new UnsupportedOperationException("Exp with type: " + r.getClass() + " not supported!");
  }
}