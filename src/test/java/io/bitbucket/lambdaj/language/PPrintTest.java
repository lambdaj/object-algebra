package io.bitbucket.lambdaj.language;

import io.bitbucket.lambdaj.language.ops.pprint.LispPrintOp;
import io.bitbucket.lambdaj.language.ops.pprint.PPrint;
import io.bitbucket.lambdaj.language.ops.pprint.PPrintOp;
import org.junit.Test;

import static io.bitbucket.lambdaj.language.cases.Language.add;
import static io.bitbucket.lambdaj.language.cases.Language.lint;
import static io.bitbucket.lambdaj.language.cases.Language.mul;
import static org.junit.Assert.assertEquals;

public class PPrintTest {
  final LanguageObjAlgDef<PPrint> lisp = new LispPrintOp();
  final LanguageObjAlgDef<PPrint> pprint = new PPrintOp();

  @Test
  public void testLispPPrint_int() throws Exception {
    assertEquals("3", lisp.visit(lint(3)).get());
  }

  @Test
  public void testLispPPrint_plus() throws Exception {
    assertEquals("(+ 2 1)", lisp.visit(add(lint(2), lint(1))).get());
  }

  @Test
  public void testLispPPrint_mul() throws Exception {
    assertEquals("(* 2 1)", lisp.visit(mul(lint(2), lint(1))).get());
  }

  @Test
  public void testLispPPrint_complex() throws Exception {
    assertEquals("(+ 2 (* 3 1))", lisp.visit(add(lint(2), mul(lint(3), lint(1)))).get());
  }

  @Test
  public void testPPrint_int() throws Exception {
    assertEquals("3", pprint.visit(lint(3)).get());
  }

  @Test
  public void testPPrint_plus() throws Exception {
    assertEquals("2 + 1", pprint.visit(add(lint(2), lint(1))).get());
  }

  @Test
  public void testPPrint_mul() throws Exception {
    assertEquals("2 * 1", pprint.visit(mul(lint(2), lint(1))).get());
  }

  @Test
  public void testPPrint_complex() throws Exception {
    assertEquals("2 + 3 * 1", pprint.visit(add(lint(2), mul(lint(3), lint(1)))).get());
  }
}