package io.bitbucket.lambdaj.language.cases;

public abstract class Language {
  private Language() {

  }

  public static Int lint(final int value) {
    return new Int(value);
  }

  public static Add add(final Exp x, final Exp y) {
    return new Add(x, y);
  }

  public static Mul mul(final Exp x, final Exp y) {
    return new Mul(x, y);
  }
}
