package io.bitbucket.lambdaj.language.cases;

import java.util.Objects;
import java.util.function.Function;

public class Int implements Exp {
  private final int value;

  public Int(final int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public Int map(final Function<Integer, Integer> f) {
    return new Int(f.apply(value));
  }

  public Int flatMap(final Function<Integer, Int> f) {
    return f.apply(value);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final Int anInt = (Int) o;
    return Objects.equals(value, anInt.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }

  @Override
  public String toString() {
    return "Int{" +
      "value=" + value +
      '}';
  }
}
