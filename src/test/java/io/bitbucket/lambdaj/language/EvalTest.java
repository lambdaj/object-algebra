package io.bitbucket.lambdaj.language;


import io.bitbucket.lambdaj.language.ops.pprint.Eval;
import io.bitbucket.lambdaj.language.ops.pprint.EvalOp;
import org.junit.Test;

import static io.bitbucket.lambdaj.language.cases.Language.add;
import static io.bitbucket.lambdaj.language.cases.Language.lint;
import static io.bitbucket.lambdaj.language.cases.Language.mul;
import static org.junit.Assert.assertEquals;

public class EvalTest {
  final LanguageObjAlgDef<Eval> eval = new EvalOp();

  @Test
  public void testLispPPrint_int() throws Exception {
    assertEquals(lint(3), eval.visit(lint(3)).get());
  }

  @Test
  public void testLispPPrint_plus() throws Exception {
    assertEquals(lint(3), eval.visit(add(lint(2), lint(1))).get());
  }

  @Test
  public void testLispPPrint_mul() throws Exception {
    assertEquals(lint(2), eval.visit(mul(lint(2), lint(1))).get());
  }

  @Test
  public void testLispPPrint_complex() throws Exception {
    assertEquals(lint(5), eval.visit(add(lint(2), mul(lint(3), lint(1)))).get());
  }
}